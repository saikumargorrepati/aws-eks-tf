output "endpoint" {
  value = aws_eks_cluster.testenv.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.testenv.certificate_authority[0].data
}
output "cluster_id" {
  value = aws_eks_cluster.testenv.id
}
output "cluster_endpoint" {
  value = aws_eks_cluster.testenv.endpoint
}
output "cluster_name" {
  value = aws_eks_cluster.testenv.name
}
