###### vpc/outputs.tf 
output "aws_public_subnet" {
  value = aws_subnet.public_testenv_subnet.*.id
}

output "vpc_id" {
  value = aws_vpc.testenv.id
}
